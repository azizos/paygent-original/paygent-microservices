package org.accenture.paygent.memberservice.datasource;

import org.accenture.paygent.memberservice.models.entities.Member;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface MemberRepo extends CrudRepository<Member, String> {
}

