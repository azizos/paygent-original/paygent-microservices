package org.accenture.paygent.groupservice.datasource;

import org.accenture.paygent.groupservice.models.entities.Group;
import org.springframework.data.repository.CrudRepository;

public interface GroupRepo extends CrudRepository<Group, String> {
    Group findByTitle(String name);
}

