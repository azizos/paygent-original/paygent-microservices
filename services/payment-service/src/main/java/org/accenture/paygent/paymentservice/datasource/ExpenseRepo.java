package org.accenture.paygent.paymentservice.datasource;

import org.accenture.paygent.paymentservice.models.entities.Expense;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExpenseRepo extends CrudRepository<Expense, String> {
}