package org.accenture.paygent.paymentservice.models.entities;

public enum Currency {
    EUR, USD
}
