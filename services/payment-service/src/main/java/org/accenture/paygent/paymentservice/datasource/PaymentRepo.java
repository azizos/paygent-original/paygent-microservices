package org.accenture.paygent.paymentservice.datasource;

import org.accenture.paygent.paymentservice.models.entities.Payment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepo extends CrudRepository<Payment, String> {
}

