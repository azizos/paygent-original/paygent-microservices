package org.accenture.paygent.paymentservice.services;

import org.accenture.paygent.paymentservice.datasource.ExpenseRepo;
import org.accenture.paygent.paymentservice.models.entities.Expense;
import org.accenture.paygent.paymentservice.services.generics.GenericAbstractService;
import org.springframework.stereotype.Service;

@Service
public class ExpenseService extends GenericAbstractService<Expense, ExpenseRepo> {

}
